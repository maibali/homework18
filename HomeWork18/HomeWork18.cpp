#include <iostream>

using namespace std;


class Stack
{
private:
	

public:
	void FillArray(int* const arr, const int size) //�����, ������� ������� ���������� ��� �������
	{
		for (int i = 0; i < size; i++)
		{
			arr[i] = rand() % 10;
		}
	}

	void ShowArray(int* const arr, const int size)  // �����, ������� ������� ��� ���������� �������
	{
		for (int i = 0; i < size; i++)
		{
			cout << arr[i] << "\t";
		}
		cout << endl;
	}

	void push(int*& arr, int& size, int value)  //�������� ������ �������, ��� ���������� ������ �������� � ����� �������
	{
		int* newArray = new int[size + 1];
		for (int i = 0; i < size; i++)
		{
			newArray[i] = arr[i];
		}
		newArray[size] = value;  //���������� �������� � ����� �������
		size++;

		delete[] arr;  //�������� � ������ ������� �������
		arr = newArray; //��������� ��������� �� ����� �������
	}

	void pop(int*& arr, int& size)  //�������� ������ �������, ��� �������� ���������� �������� �������
	{
		size--;
		int* newArray = new int[size];
		for (int i = 0; i < size; i++)
		{
			newArray[i] = arr[i];
		}


		delete[] arr;  //�������� � ������ ������� �������
		arr = newArray; //��������� ��������� �� ����� �������
	}
};



int main()
{

	int size = 5;
	int* arr = new int[size];

	Stack a;

	a.FillArray(arr, size);
	a.ShowArray(arr, size);

	a.pop(arr, size);
	a.ShowArray(arr, size);

	a.push(arr, size, 111);
	a.ShowArray(arr, size);

	a.pop(arr, size);
	a.ShowArray(arr, size);

}
    
 
